package model

import (
	"fmt"
	"regexp"
	"strconv"
	"time"
)

type upload struct{}

var UploadModel upload

var (
	// name- unixTimestamp
	regexNewImageFilename = regexp.MustCompile(`^(.+)-(\d+)$`)
	// name- unixTimestamp-shape-width
	regexOldImageFilename = regexp.MustCompile(`^.+-(\d+)-(o|s)-(\d+)$`)
	//  unixTimestamp_name
	regexUploadFilename = regexp.MustCompile(`^(\d+)_(.+)$`)
)

type FileName struct {
	value     string
	timestamp time.Time
}

type MUpload interface {
	ParseUploadFileName(fileName string) (string, time.Time, error)
	ParseImageFileName(fileName string) (string, time.Time, error)
}

func (u upload) ParseUploadFileName(fileName string) (name string, timestamp time.Time, err error) {
	values := regexUploadFilename.FindStringSubmatch(fileName)
	if len(values) != 3 {
		err = fmt.Errorf("Invalid Format ParseUploadFileName")
		return
	}

	name, timestamp, err = getFileImage(values[1], values[2])
	return
}

func (u upload) ParseImageFileName(fileName string) (name string, timestamp time.Time, err error) {
	values := regexNewImageFilename.FindStringSubmatch(fileName)
	if len(values) == 0 {
		err = fmt.Errorf("Invalid Format ParseImageFileName")
	}

	_, timestamp, err = getFileImage(values[2], string(values[1]))
	return values[0], timestamp, err
}

func getFileImage(nanoTime, name string) (value string, timestamp time.Time, err error) {
	timestamp, err = parseTimestamp(nanoTime)
	if err != nil {
		err = fmt.Errorf("Invalid parseTimestamp")
		return
	}
	//	file = &FileName{
	//		value:     name,
	//		timestamp: timestamp,
	//	}
	return name, timestamp, err

}

func parseTimestamp(value string) (ret time.Time, err error) {
	var sec int64
	sec, err = strconv.ParseInt(value, 10, 64)
	if err != nil {
		return
	}
	ret = time.Unix(sec, 0)
	return
}
