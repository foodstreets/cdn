package handler

import (
	"fmt"
	"net/http"
	"strconv"

	"gitlab.com/foodstreets/cdn/api/entity"
	"gitlab.com/foodstreets/cdn/api/model"

	"github.com/gin-gonic/gin"
)

var (
	ErrorMethodNotAllowed = "Method not allowed"
	ErrorImageNotProvided = "image_file field not provided"
	ErrorFileIsNotImage   = "Provided file is not an accepted image"
	ErrorInvalidToken     = "Invalid Token"
	ErrorImageNotFound    = "Image not found"
	ErrorAddressNotFound  = "Address not found"
	ErrorServerError      = "Internal Server Error"
)

const (
	MAX_UPLOAD_SIZE = 1024 * 1024 // 1MB
)

type upload struct{}

var UploadHandler IUpload

type IUpload interface {
	Upload(c *gin.Context)
	GetOriginal(c *gin.Context)
	Move(c *gin.Context)
	GetImage(c *gin.Context)
}

func init() {
	UploadHandler = &upload{}
}

func (u upload) Upload(c *gin.Context) {
	//Valid request size upload file
	c.Request.Body = http.MaxBytesReader(c.Writer, c.Request.Body, MAX_UPLOAD_SIZE)
	if err := c.Request.ParseMultipartForm(MAX_UPLOAD_SIZE); err != nil {
		jsonResponse(c, http.StatusBadRequest, "The uploaded file is too big. Please choose an file that's less than 1MB in size")
		return
	}

	file, fileHeader, err := c.Request.FormFile("file")
	if err != nil {
		jsonResponse(c, http.StatusInternalServerError, err.Error())
		return
	}
	defer file.Close()

	mimeType := fileHeader.Header.Get("Content-Type")
	if valid := validateImage(mimeType); !valid {
		jsonResponse(c, http.StatusBadRequest, ErrorFileIsNotImage)
		return
	}

	name, err := entity.Upload.Upload(file, fileHeader)
	if err != nil {
		jsonResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"name": name,
	})
}

func (u upload) Move(c *gin.Context) {
	imageName, timestamp, err := model.UploadModel.ParseImageFileName(c.Query("name"))
	if err != nil {
		jsonResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	err = entity.Upload.Move(c.Param("urlTmp"), imageName, timestamp)
	if err != nil {
		jsonResponse(c, http.StatusInternalServerError, err.Error())
		return
	}
}

func (u upload) GetOriginal(c *gin.Context) {
	_, timestamp, err := model.UploadModel.ParseUploadFileName(c.Param("name"))
	if err != nil {
		jsonResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	filePath, err := entity.Upload.GetOriginal(c.Param("name"), timestamp)
	if err != nil {
		jsonResponse(c, http.StatusBadRequest, err.Error())
		return
	}
	fmt.Println("filePath", filePath)

	c.File(filePath)
}

func (u upload) GetImage(c *gin.Context) {
	nameParam := c.Param(nameParam)
	qWith, _ := strconv.Atoi(c.Query(withQuery))
	qHeight, _ := strconv.Atoi(c.Query(heightQuery))
	qType := c.Query(typeQuery)

	imageName, timestamp, err := model.UploadModel.ParseImageFileName(nameParam)
	if err != nil {
		jsonResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	filePath, err := entity.Upload.GetImage(imageName, qType, qWith, qHeight, timestamp)
	if err != nil {
		jsonResponse(c, http.StatusBadRequest, err.Error())
		return
	}
	c.File(filePath)
}
