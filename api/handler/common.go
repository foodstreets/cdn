package handler

import (
	"fmt"
	"path/filepath"

	"github.com/gin-gonic/gin"
)

const (
	nameParam = "name"

	withQuery   = "with"
	heightQuery = "height"
	typeQuery   = "type"
)

func validateImage(mimeType string) bool {
	switch mimeType {
	case "image/jpeg", "image/png", "image/jpg":
		return true
	default:
		return false
	}
}

func getFilePathFromImageID(dataDir string, imageID string) string {
	parentDir := fmt.Sprintf("./images/%s/%s", imageID[1:2], imageID[3:5])
	parentDir = filepath.Join(dataDir, parentDir)
	return fmt.Sprintf("%s/%s", parentDir, imageID)
}

func jsonResponse(c *gin.Context, status int, message interface{}) {
	c.JSON(status, gin.H{
		"message": message,
	})
}
