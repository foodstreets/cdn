package entity

import (
	"image"
	"image/color"
	"log"
	"math"
	"mime/multipart"
	"os"
	"path"
	"strconv"
	"strings"
	"time"

	"gitlab.com/foodstreets/cdn/lib/imaging"
)

type upload struct{}

var Upload IUpload

type IUpload interface {
	Upload(file multipart.File, fileHeader *multipart.FileHeader) (string, error)
	GetImage(imageName, imageType string, with, height int, timestamp time.Time) (string, error)
	Move(urlTmp, imageName string, timestamp time.Time) error
	GetOriginal(filePath string, timestamp time.Time) (string, error)
}

var (
	tmpPathDir    = "/Users/caonam/go/src/gitlab.com/foodstreets/cdn/resources/tmp/"
	originPathDir = "/Users/caonam/go/src/gitlab.com/foodstreets/cdn/resources/images/"
	resizePathDir = "/Users/caonam/go/src/gitlab.com/foodstreets/cdn/resources/resize/images/"
	cropPathDir   = "/Users/caonam/go/src/gitlab.com/foodstreets/cdn/resources/crop/images/"
)

const (
	defaultWith = 240
)

func init() {
	Upload = &upload{}
}

func getInfoImage(imagePath string) (InfoImage, error) {
	reader, err := os.Open(imagePath)
	defer reader.Close()
	if err != nil {
		return InfoImage{}, err
	}

	image, filePathExt, err := image.DecodeConfig(reader)
	if err != nil {
		return InfoImage{}, err
	}
	return InfoImage{
		Width:       image.Width,
		Height:      image.Height,
		FilePathExt: filePathExt,
	}, nil
}

func getImageFormatExt(imageType string) OptionType {
	if len(imageType) == 0 {
		return RISIZE
	}

	if o, ok := formatExts[imageType]; ok {
		return o
	}
	return RISIZE
}

func getResizeHeight(imageInfo InfoImage, with int) int {
	nWith := float64(imageInfo.Width)
	nHeight := float64(imageInfo.Height)
	ratio := nHeight / nWith
	resizeHeight := math.Ceil(ratio * float64(with))
	return int(resizeHeight)
}

func getFilePathFromImage(imageName, imageType string, with, height int, timestamp time.Time) *NewInfoImage {
	filepath := pathByTimestamp(imageName, timestamp)
	imagePath := originPathDir + filepath

	imageInfo, err := getInfoImage(imagePath)
	if err != nil {
		return nil
	}

	if with == 0 {
		with = defaultWith
	}
	if height == 0 {
		height = getResizeHeight(imageInfo, with)
	}

	newImageType := getImageFormatExt(imageType)

	sWith := strconv.Itoa(with)
	sHeight := strconv.Itoa(height)

	pathImage := []string{imageName, newImageType.String(), sWith, sHeight}
	path := strings.Join(pathImage, "-")
	if newImageType == ORIGIN {
		path = imageName
	}

	return &NewInfoImage{
		InfoImage: InfoImage{
			Width:       with,
			Height:      height,
			FilePathExt: imageInfo.FilePathExt,
		},
		Path: path,
	}
}

func (u upload) GetImage(imageName, imageType string, with, height int, timestamp time.Time) (filePath string, err error) {
	var imagePathDir, pathImageSave string
	newInfoImage := getFilePathFromImage(imageName, imageType, with, height, timestamp)
	if newInfoImage == nil {
		return
	}

	filepath := pathByTimestamp(newInfoImage.Path, timestamp)
	newImageType := getImageFormatExt(imageType)
	if newImageType == CROP {
		imagePathDir = cropPathDir
	} else if newImageType == ORIGIN {
		imagePathDir = originPathDir
	} else {
		imagePathDir = resizePathDir
	}

	imagePath := path.Join(imagePathDir, filepath)
	pathImageSave = strings.Join([]string{imagePath, newInfoImage.FilePathExt}, ".")
	if newImageType == ORIGIN {
		return imagePath, nil
	}

	src, _ := imaging.Open(pathImageSave)
	if src != nil {
		return pathImageSave, nil
	}

	// Create folder
	err = EnsureBaseDir(imagePath)
	if err != nil {
		return
	}

	dst := destinationImage(imageName, imageType, newInfoImage, with, timestamp)
	if dst == nil {
		return
	}

	// Save the resulting image as JPEG.
	err = imaging.Save(dst, pathImageSave)
	if err != nil {
		log.Fatalf("failed to save image: %v", err)
	}
	return pathImageSave, nil
}

func destinationImage(imageName, imageType string, newInfoImage *NewInfoImage, with int, timestamp time.Time) (img *image.NRGBA) {
	if newInfoImage == nil {
		return
	}

	fileOriginPath := pathByTimestamp(imageName, timestamp)
	srcOriginPath := path.Join(originPathDir, fileOriginPath)
	srcOrigin, _ := imaging.Open(srcOriginPath)

	newWith := newInfoImage.Width
	newHeight := newInfoImage.Height

	newImageType := getImageFormatExt(imageType)
	newSrc := &image.NRGBA{}
	if newImageType == CROP {
		newSrc = imaging.CropAnchor(srcOrigin, newWith, newHeight, imaging.Center)
	} else {
		newSrc = imaging.Resize(srcOrigin, newWith, newHeight, imaging.NearestNeighbor)
	}

	img1 := imaging.Blur(newSrc, 0)
	img2 := imaging.Grayscale(newSrc)
	img2 = imaging.AdjustContrast(img2, 20)
	img2 = imaging.Sharpen(img2, 0.5)

	// Create an inverted version of the image.
	img3 := imaging.Invert(newSrc)

	dst := imaging.New(newWith, newHeight, color.NRGBA{0, 0, 0, 0})
	dst = imaging.Paste(dst, img1, image.Pt(0, 0))
	dst = imaging.Paste(dst, img2, image.Pt(newWith, newHeight))
	dst = imaging.Paste(dst, img3, image.Pt(newWith, 0))
	return dst
}

func (u upload) Upload(file multipart.File, fileHeader *multipart.FileHeader) (name string, err error) {
	name, filepath := pathFolder()
	tmpPath := tmpPathDir + filepath
	err = EnsureBaseDir(tmpPath)
	if err != nil {
		return
	}

	err = createFile(tmpPath, file)
	return
}

func (u upload) Move(urlTmp, imageName string, timestamp time.Time) (err error) {
	filepath := pathByTimestamp(imageName, timestamp)
	imagePath := originPathDir + filepath
	err = EnsureBaseDir(imagePath)
	if err != nil {
		return
	}
	tmpPath := tmpPathDir + pathByTimestamp(urlTmp, time.Now())
	f, err := os.Open(tmpPath)
	if err != nil {
		return
	}

	err = createFile(imagePath, f)
	return
}

func (u upload) GetOriginal(filePath string, timestamp time.Time) (string, error) {
	path := pathByTimestamp(filePath, timestamp)
	return originPathDir + path, nil
}
