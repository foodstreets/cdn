package entity

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"io"
	"mime/multipart"
	"os"
	"path"
	"strconv"
	"strings"
	"time"

	uuid "github.com/satori/go.uuid"
)

type InfoImage struct {
	Width       int
	Height      int
	FilePathExt string
}

type NewInfoImage struct {
	InfoImage
	Path string
}

// Format is an image file format.
type OptionType string

// Image option formats.
const (
	RISIZE OptionType = "r"
	CROP   OptionType = "c"
	ORIGIN OptionType = "o"
)

var formatExts = map[string]OptionType{
	"r": RISIZE,
	"c": CROP,
	"o": ORIGIN,
}

var formatNames = map[OptionType]string{
	RISIZE: "resize",
	CROP:   "crop",
	ORIGIN: "origin",
}

func (o OptionType) String() string {
	return formatNames[o]
}

func createFile(tmpPath string, file multipart.File) (err error) {

	// Create a new file in the uploads directory
	dst, err := os.Create(tmpPath)
	if err != nil {
		return
	}
	defer dst.Close()

	// Copy the uploaded file to the filesystem
	// at the specified destination
	_, err = io.Copy(dst, file)
	return err
}

func uniqueStr(sec int64) string {
	hash := md5.New()
	uid := strings.Replace(uuid.NewV4().String(), "-", "", -1)
	io.WriteString(hash, fmt.Sprintf("%s%v", uid, sec))
	return hex.EncodeToString(hash.Sum(nil))
}

func pathByTimestamp(name string, timestamp time.Time) string {
	year, month, day := strconv.Itoa(timestamp.Year()),
		strconv.Itoa(int(timestamp.Month())),
		strconv.Itoa(timestamp.Day())
	return path.Join(year, month, day, name)
}

func pathFolder() (string, string) {
	uuid := uniqueStr(time.Now().UnixNano())
	timestamp := time.Now()
	name := fmt.Sprintf("%d_%s", timestamp.Unix(), uuid)
	filepath := pathByTimestamp(name, timestamp)
	return name, filepath
}

func EnsureBaseDir(fpath string) error {
	baseDir := path.Dir(fpath)
	info, err := os.Stat(baseDir)
	if err == nil && info.IsDir() {
		return nil
	}
	return os.MkdirAll(baseDir, 0755)
}
