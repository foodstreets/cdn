package route

import (
	"gitlab.com/foodstreets/cdn/api/handler"

	"github.com/gin-gonic/gin"
)

var (
	version = "version"
)

func Init() *gin.Engine {
	r := gin.Default()

	handlerUpload := handler.UploadHandler

	groupVersion := r.Group(":" + version)
	upload := groupVersion.Group("/upload")
	{
		upload.POST("/images", handlerUpload.Upload)
		upload.POST("/images/:urlTmp/move", handlerUpload.Move)
		upload.GET("/images/original/:name", handlerUpload.GetOriginal)
	}

	resize := groupVersion.Group("/food")
	{
		resize.GET("/image/:name", handlerUpload.GetImage)
	}

	return r
}
