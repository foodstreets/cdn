FROM golang

RUN mkdir -p /go/src

COPY . /go/src

WORKDIR /go/src
RUN go mod download

RUN go build -o /fs-cdn

EXPOSE 8084

CMD [ "/fs-cdn" ]
